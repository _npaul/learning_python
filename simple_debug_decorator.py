from functools import wraps
import logging

def debug(func):
    log = logging.getLogger(func.__module__)
    msg = f"Running {func.__qualname__}"
    # Copy metadata: name, docstring, function attributes
    @wraps(func)
    def wrapper(*args, **kwargs):
        log.debug(msg)
        return func(*args, **kwargs)
    return wrapper


@debug
def add(x, y):
    return x+y

four = add(1, 3)

    