def say_hello(name: str):
    print(f"Hello {name}!")

say_hello("Nick")
say_hello(1)