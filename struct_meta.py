class point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def my_init(self, x, y):
    self.x = x
    self.y = y

point2 = type('point2', (), {'__init__': my_init})

def generic_init(names, self, *args):
    for name, arg in zip(names, args):
        setattr(self, name, arg)

point3 = type('point3', (), {
    '__init__': lambda self, *args: generic_init(['x', 'y'], self, *args)
})

def struct(name, arg_names):

    def init(names, self, *args):
        for name, arg in zip(names, args):
            setattr(self, name, arg)

    globals()[name] = type(name, (), {
        '__init__': lambda self, *args: init(arg_names, self, *args)
    })

struct('point4', ('x', 'y'))