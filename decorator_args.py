from functools import wraps, partial

def debug(prefix=''):
    def decorate(func):
        msg = f"{prefix}Running {func.__qualname__}"
        # Copy metadata: name, docstring, function attributes
        @wraps(func)
        def wrapper(*args, **kwargs):
            print(msg)
            return func(*args, **kwargs)
        return wrapper
    return decorate


def debug2(func=None, *, prefix=''):
    if func is None:
        return partial(debug2, prefix=prefix)

    msg = f"{prefix}Running {func.__qualname__}"
    # Copy metadata: name, docstring, function attributes
    @wraps(func)
    def wrapper(*args, **kwargs):
        print(msg)
        return func(*args, **kwargs)
    return wrapper

    


@debug(prefix='***')
def add(x, y):
    return x+y

add(1, 3)

# debug2 can be used as a simple decorator or with an argument

@debug2(prefix='!!!')
def sub(x, y):
    return x-y

sub(1, 3)

@debug2
def mul(x, y):
    return x*y

mul(1, 3)


    